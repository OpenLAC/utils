! Function to load and call dll functions
! - Windows:
!   - Intel fortran: Included in the compiler. Do not use this or any other module
!   - gfortran: Compile with this dfwin_gfortran.f90
! - Linux:
!   - Intel fortran: Compile with this dfwin_linux.f90

!DEC$ IF DEFINED(__linux__) !! preprocessor #ifdef gives error in cmake compilation order
module dfwin
    implicit none

    ! --------------------------------------------------------------------------------
    ! ---  Interface to linux API
    ! --------------------------------------------------------------------------------
    interface
        function dlopen(filename,mode) bind(c,name="dlopen")
            ! void *dlopen(const char *filename, int mode);
            use iso_c_binding, only: C_INTPTR_T
            use iso_c_binding, only: C_INT, C_CHAR
            integer(C_INTPTR_T) :: dlopen
            character(C_CHAR),  dimension(*),intent(in) :: filename
            integer(C_INT), value :: mode
        end function

        function dlsym(handle,name) bind(c,name="dlsym")
            ! void *dlsym(void *handle, const char *name);
            use iso_c_binding, only: C_INTPTR_T
            use iso_c_binding, only: C_CHAR
            integer(C_INTPTR_T)        :: dlsym
            integer(C_INTPTR_T), value :: handle
            character(C_CHAR), dimension(*), intent(in) :: name
        end function

        function dlclose(handle) bind(c,name="dlclose")
            ! int dlclose(void *handle);
            use iso_c_binding, only: C_INTPTR_T
            integer(C_INTPTR_T)        :: dlclose
            integer(C_INTPTR_T), value :: handle
        end function
    end interface

    private
    public :: loadlibrary, getprocaddress, freelibrary


contains
    ! --------------------------------------------------------------------------------
    ! --- Fundamentals 
    ! --------------------------------------------------------------------------------
    function loadlibrary( libfile ) result(file_address)
        !!gcc$ attributes stdcall :: loadlibrary 
        use iso_c_binding, only: C_INTPTR_T
        use iso_c_binding, only: C_INT
        ! Function result
        integer(C_INTPTR_T) :: file_address
        ! Arguments
        character(*) :: libfile
        ! Variables
        integer(C_INT), parameter :: rtld_lazy=1
        !
        file_address = dlopen( libfile, rtld_lazy )
    end function loadlibrary

    function getprocaddress( libhandle, procedure_name )
        !        !gcc$ attributes stdcall :: getprocaddress
        use iso_c_binding, only: C_INTPTR_T
        integer(C_INTPTR_T) :: getprocaddress
        ! Arguments
        integer(C_INTPTR_T) :: libhandle
        character(*) :: procedure_name
        getprocaddress = dlsym( libhandle, procedure_name )
    end function getprocaddress

    function freelibrary( libhandle )
        use iso_c_binding, only: C_INTPTR_T
        logical :: freelibrary
        ! Arguments
        integer(C_INTPTR_T) :: libhandle
        ! Variables
        integer :: err

        err = dlclose( libhandle )
        freelibrary = ( err .eq. 0 )
        return
    end function freelibrary

    ! --------------------------------------------------------------------------------
    ! --- Wrap functions 
    ! --------------------------------------------------------------------------------
    !function loadsymbol(libhandle,s)
    !    use iso_c_binding, only: C_INTPTR_T
    !    integer(C_INTPTR_T)  :: loadsymbol
    !    ! Arguments
    !    integer(C_INTPTR_T) :: libhandle
    !    character*(*) :: s
    !    !
    !    s(LEN_trim(s)+1:LEN_trim(s)+1) = CHAR(0)
    !    loadsymbol = getprocaddress(libhandle,s)
    !end function
end module
!DEC$ ENDIF