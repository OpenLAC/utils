'''
Created on 28. jul. 2017

@author: mmpe
'''
import os
import subprocess


def run_git_cmd(cmd, git_repo_path=None):
    git_repo_path = git_repo_path or os.getcwd()
    if not os.path.isdir(os.path.join(git_repo_path, ".git")):
        raise Warning("'%s' does not appear to be a Git repository." % git_repo_path)
    try:
        process = subprocess.Popen(cmd,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE,
                                   universal_newlines=True,
                                   cwd=os.path.abspath(git_repo_path))
        stdout, stderr = process.communicate()
        if process.returncode != 0:
            raise EnvironmentError("%s\n%s" % (stdout, stderr))
        return stdout.strip()

    except EnvironmentError as e:
        raise e
        raise Warning("unable to run git")


def get_git_version(git_repo_path=None):
    cmd = ["git", "describe", "--tags", "--dirty", "--always"]
    return run_git_cmd(cmd, git_repo_path)


def get_tag(git_repo_path=None, verbose=False):
    tag = run_git_cmd(['git', 'describe', '--tags', '--always', '--abbrev=0'], git_repo_path)
    if verbose:
        print(tag)
    return tag


def set_tag(tag, push, git_repo_path=None):
    run_git_cmd(["git", "tag", tag], git_repo_path)
    if push:
        run_git_cmd(["git", "push"], git_repo_path)
        run_git_cmd(["git", "push", "--tags"], git_repo_path)


def update_git_version(version_module, git_repo_path=None):
    """Update <version_module>.__version__ to git version"""

    version_str = get_git_version(git_repo_path)
    assert os.path.isfile(version_module.__file__)
    with open(version_module.__file__, "w") as fid:
        fid.write("__version__ = '%s'" % version_str)

    # ensure file is written, closed and ready
    with open(version_module.__file__) as fid:
        fid.read()
    return version_str


def format_log():
    cwd = os.path.dirname(__file__) + '/../../'
    tag = run_git_cmd('git describe --tags --abbrev=0', cwd)

    log = run_git_cmd('git log %s..head --pretty=format:"!#!%%ci  %%an  %%B"' % tag, cwd)

    def fmt(l):

        date = l[:10]
        message = l[25:].strip().split("\n")
        author = message[0].strip().split(" ")[0]
        first_line = "- %s %s" % (date, message[0])
        next_lines = [(" " * (len(author) + 15)) + m for m in message[1:]]
        return "\n".join([first_line] + next_lines)
    print("\n".join([fmt(l) for l in log.split("!#!")[::-1] if l.strip()]))


if __name__ == '__main__':
    format_log()
