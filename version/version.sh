GIT_ID=$(git describe --tags --dirty --always)
GIT_VERSION=$(git describe --tags --abbrev=0)
GIT_BRANCH=$(git branch  --show-current)

if [ "$GIT_ID" != "$GIT_VERSION" ]; then
  GIT_VERSION="$GIT_VERSION,-1"
fi
GIT_VERSION=${GIT_VERSION//./,}



echo "#define GIT_ID \"$GIT_ID\"
#define GIT_VERSION \"$GIT_VERSION\"
#define GIT_BRANCH \"$GIT_BRANCH\"
#define USERNAME \"$(whoami)\"
#define COMPUTERNAME \"$HOSTNAME\"
#define BUILD_DATE \"$(date +%Y-%m-%d)\"
#define BUILD_TYPE \"$1 $2\"" > version.h


echo "---------------------"
cat version.h
echo "---------------------"
