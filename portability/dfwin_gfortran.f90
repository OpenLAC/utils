! Functions to interface windows API and Linux APIs which are compitable 
! with gfortran comipler!
!
! Currently, it implements load and call dll functions and GetModuleFileName
! - Windows:
!   - gfortran: Compile with this dfwin_gfortran.f90
! - Linux:
!   - gfortran: Compile with this dfwin_gfortran.f90

module dfwin
    

! -----------------------------------------------------------
! --------  Interface to Linux API
! -----------------------------------------------------------
#ifdef _LINUX

	implicit none

    ! --------------------------------------------------------------------------------
    ! ---  Interface to linux API
    ! --------------------------------------------------------------------------------
    interface
        function dlopen(filename,mode) bind(c,name="dlopen")
            ! void *dlopen(const char *filename, int mode);
            use iso_c_binding, only: C_INTPTR_T
            use iso_c_binding, only: C_INT, C_CHAR
            integer(C_INTPTR_T) :: dlopen
            character(C_CHAR),  dimension(*),intent(in) :: filename
            integer(C_INT), value :: mode
        end function

        function dlsym(handle,name) bind(c,name="dlsym")
            ! void *dlsym(void *handle, const char *name);
            use iso_c_binding, only: C_INTPTR_T
            use iso_c_binding, only: C_CHAR
            integer(C_INTPTR_T)        :: dlsym
            integer(C_INTPTR_T), value :: handle
            character(C_CHAR), dimension(*), intent(in) :: name
        end function

        function dlclose(handle) bind(c,name="dlclose")
            ! int dlclose(void *handle);
            use iso_c_binding, only: C_INTPTR_T
            integer(C_INTPTR_T)        :: dlclose
            integer(C_INTPTR_T), value :: handle
        end function
    end interface

    private
    public :: loadlibrary, getprocaddress, freelibrary


	contains
    ! --------------------------------------------------------------------------------
    ! --- Fundamentals 
    ! --------------------------------------------------------------------------------
    function loadlibrary( libfile ) result(file_address)
        !!gcc$ attributes stdcall :: loadlibrary 
        use iso_c_binding, only: C_INTPTR_T
        use iso_c_binding, only: C_INT
        ! Function result
        integer(C_INTPTR_T) :: file_address
        ! Arguments
        character(*) :: libfile
        ! Variables
        integer(C_INT), parameter :: rtld_lazy=1
        !
        file_address = dlopen( libfile, rtld_lazy )
    end function loadlibrary

    function getprocaddress( libhandle, procedure_name )
        !        !gcc$ attributes stdcall :: getprocaddress
        use iso_c_binding, only: C_INTPTR_T
        integer(C_INTPTR_T) :: getprocaddress
        ! Arguments
        integer(C_INTPTR_T) :: libhandle
        character(*) :: procedure_name
        getprocaddress = dlsym( libhandle, procedure_name )
    end function getprocaddress

    function freelibrary( libhandle )
        use iso_c_binding, only: C_INTPTR_T
        logical :: freelibrary
        ! Arguments
        integer(C_INTPTR_T) :: libhandle
        ! Variables
        integer :: err

        err = dlclose( libhandle )
        freelibrary = ( err .eq. 0 )
        return
    end function freelibrary
  
#else
  USE, INTRINSIC :: ISO_C_BINDING, ONLY:  &
      C_F_PROCPOINTER, C_FUNPTR, C_INTPTR_T,  &
      C_NULL_CHAR, C_CHAR, C_ASSOCIATED, C_LONG 

  implicit none

! -----------------------------------------------------------
! --------  Interface to Windows API
! -----------------------------------------------------------

  INTERFACE 
    FUNCTION LoadLibrary(lpFileName) BIND(C,NAME='LoadLibraryA')
       USE, INTRINSIC :: ISO_C_BINDING, ONLY: C_INTPTR_T, C_CHAR
       IMPLICIT NONE 
       !GCC$ ATTRIBUTES STDCALL :: LoadLibrary 
       CHARACTER(KIND=C_CHAR) :: lpFileName(*) 
       INTEGER(C_INTPTR_T) :: LoadLibrary 
    END FUNCTION LoadLibrary 

    FUNCTION GetProcAddress(hModule, lpProcName) BIND(C, NAME='GetProcAddress')
      USE, INTRINSIC :: ISO_C_BINDING, ONLY: C_FUNPTR, C_INTPTR_T, C_CHAR
      IMPLICIT NONE
      !GCC$ ATTRIBUTES STDCALL :: GetProcAddress
      TYPE(C_FUNPTR) :: GetProcAddress
      INTEGER(C_INTPTR_T), VALUE :: hModule
      CHARACTER(KIND=C_CHAR) :: lpProcName(*)
    END FUNCTION GetProcAddress
    
    FUNCTION FreeLibrary(hModule) BIND(C, NAME='FreeLibrary')
      USE, INTRINSIC :: ISO_C_BINDING, ONLY: C_FUNPTR, C_INTPTR_T, C_CHAR
      IMPLICIT NONE
      !GCC$ ATTRIBUTES STDCALL :: FreeLibrary
      logical :: FreeLibrary
      INTEGER(C_INTPTR_T), VALUE :: hModule
    END FUNCTION FreeLibrary

    function GetCurrentDirectory(nBufferLength, lpBuffer) bind(C,Name='GetCurrentDirectoryA')
      use ISO_C_BINDING
      implicit NONE
      !GCC$ ATTRIBUTES STDCALL :: GetCurrentDirectory
      integer(C_LONG) GetCurrentDirectory
      integer(C_LONG),value :: nBufferLength
      character(kind=C_CHAR) lpBuffer(*)
    end function GetCurrentDirectory

    function GetModuleFileName(hModule, lpFileName, nSize) bind(C,Name='GetModuleFileNameA')
      use ISO_C_BINDING
      implicit NONE
      !GCC$ ATTRIBUTES STDCALL :: GetModuleFileName
      integer(C_LONG) GetModuleFileName
      integer(C_INTPTR_T), value :: hModule
      character(kind=C_CHAR) lpFileName(*)
      integer(C_LONG), value :: nSize
    end function GetModuleFileName
  END INTERFACE
  private
  public :: LoadLibrary, GetProcAddress,FreeLibrary, GetCurrentDirectory, GetModuleFileName 

#endif

end module dfwin
