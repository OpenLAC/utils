!> Wrapper module to load a dll


!!!  Example of usage
!    use dll_utils
!    use iso_c_binding, only: C_INTPTR_T
!    implicit none
!    interface 
!        subroutine echo_version() BIND(C, NAME='echo_version')
!        end subroutine
!    end interface
!
!    
!#ifndef __GFORTRAN__
!       pointer(pecho_version, echo_version)
!#else
!       PROCEDURE(echo_version), BIND(C), POINTER :: pecho_version 
!#endif
!    ! Body of CheckVersion
!    
!    p = loaddll(trim(filename), 2)
!#ifndef __GFORTRAN__
!    pecho_version = loadsymbol(p, 'echo_version', 2)
!   call echo_version()
!#else
!   call C_F_PROCPOINTER(loadsymbol(p, 'echo_version',2),pecho_version) 
!   call pecho_version()
!#endif

module dll_utils
    ! dfwin defines windowsAPI for calling a dll (wrapped for linux)
    use dfwin, only: getprocaddress, loadlibrary, freelibrary

    ! Standard C Binding (not available in compaq)
    use iso_c_binding, only: C_INTPTR_T, C_CHAR, C_F_PROCPOINTER, C_NULL_FUNPTR, C_ASSOCIATED,C_FUNPTR,C_NULL_CHAR
    use filesystem_tools, only: file_exists, GetExePath, isabs
    use logging
    use StringUtils, only: string2LowerCase
    use path_utils, only: fix_path
    
    implicit none

    private

    public :: loaddll
    public :: loadsymbol
    public :: closedll
    
    interface loaddll
        module procedure loaddll1, loaddll2
    end interface
    interface loadsymbol
        module procedure loadsymbol1, loadsymbol2
    end interface

contains

    !> Loads and returns handle to a library
    integer(C_INTPTR_T) function loaddll1(s, required)
        
        interface
            subroutine get_version(s) BIND(C, NAME='get_version')
                use iso_c_binding
                implicit none
                character(kind=c_char, len=1), intent(inout)  :: s(255)
            end subroutine
        end interface
                
        ! Arguments
        character(len=*), intent(in) :: s  ! Fortran string
        integer, intent(in) :: required ! load fail=> 0: NIL, 1: Warning, 2: error
        
        ! Variables
        character(kind=C_CHAR,len=1024) :: name, name_lower, tmp, tmp_fix_path ! C-String
        integer :: i_ext, i_pre,i_end_pre, i_fix_path
        character(kind=c_char, len=1)  :: version_arr(255) = ''
        character(len=256)  :: version_str=''
#ifndef __GFORTRAN__
        pointer (pgetversion,get_version)
#else
        PROCEDURE(get_version), BIND(C), POINTER :: pgetversion 
        TYPE(C_FUNPTR) :: proc_address
        
#endif
        character(len=7) :: ext(3) = (/'_64.dll', '.dll   ','.so    '/) ! all extentions
        character(len=256) :: pre(3) = (/ "","" , "" /)
        


#ifdef _WIN64
        integer :: i1=1,i2=2
#elif _WIN32
        integer :: i1=2,i2=2
#else
        integer :: i1=3,i2=3
#endif

        ! remove the last null character, if there
        if ( s(len_trim(s):len_trim(s)) == C_NULL_CHAR ) then
          name = s(1:len_trim(s)-1)
        else
          name = trim(s)
        end if
        
        ! remove current extension from dll name
        name_lower = string2lowercase(name)
        do i_ext = 1, size(ext)
            if (name_lower(len_trim(s)-len_trim(ext(i_ext))+1:len_trim(name))==ext(i_ext)) then 
                name(len_trim(s)-len_trim(ext(i_ext))+1:len_trim(name)) = ""
            end if
        enddo
        
        loaddll1 = 0
        if (isabs(name) )then
            i_end_pre = 1
        else
            i_end_pre=3
            call getcwd(pre(2)) 
            pre(2) = trim(pre(2))//"/"
            call getexepath(pre(3))
        endif
        
        
        do i_fix_path = 1,2 ! 1: do not fix path, 2: fix path (replace(\,/) and fix case)
            do i_ext = i1,i2 ! loop over system specific extensions
                do i_pre = 1,i_end_pre ! loop over prefixes ('', cwd, exe_path)   
                    tmp = trim(pre(i_pre))//trim(name)//trim(ext(i_ext))
                    if (i_fix_path==2) then 
                        tmp_fix_path = fix_path(trim(pre(i_pre)), trim(name)//trim(ext(i_ext)))
                        if (tmp_fix_path==tmp) then
                            cycle
                        else
                            tmp = tmp_fix_path
                        endif
                    endif
                    loaddll1 = loadlibrary(trim(tmp)//CHAR(0))
                    if (loaddll1==0) then
                        if (.not. file_exists(tmp)) then
                            call log_info('DLL does not exist: ' //trim(tmp))
                        else
                            call log_info('DLL loading failed: ' //trim(tmp))
                        end if
                    else
                        exit
                    endif
                end do
                if (loaddll1/=0) exit
            enddo
            if (loaddll1/=0) exit
        enddo
        if (loaddll1/=0) then
            call log_info('DLL loaded with success ' // trim(tmp))
            version_arr(:) = ""
#ifndef __GFORTRAN__
            ! load succeeded get version info
            pgetversion = loadsymbol(loaddll1,'get_version',0)
            if (pgetversion/=0) then
                call get_version(version_arr)
#else
            proc_address = loadsymbol(loaddll1,'get_version'//CHAR(0),0)
            if (C_ASSOCIATED(proc_address)) then 
                call C_F_PROCPOINTER(proc_address,pgetversion) 
                call pgetversion(version_arr)
#endif
                ! new version interface
                version_str = ''
                call cstring2fortran(version_arr,version_str)    
            else
                version_str='unknown'
            end if
            
            call log_info('Using ' //trim(tmp) // ', version: ' // trim(version_str))
        endif

        if (required==2 .and. loaddll1 == 0) then
            call log_error("DLL load failed with error")
        elseif(required==1 .and. loaddll1 == 0) then
            call log_warning("DLL load failed with warning")
        end if
 
    end function

    integer(C_INTPTR_T) function loaddll2(s)
        character(len=*), intent(in)       :: s         !< Procedure name (Fortran string)
        loaddll2 = loaddll1(s,1)
    end function


#ifdef __GFORTRAN__
    TYPE(C_FUNPTR) function loadsymbol1(libhandle,s,required) 
#else
    integer(C_INTPTR_T) function loadsymbol1(libhandle,s,required)
#endif
        ! Arguments
        integer(C_INTPTR_T), intent(inout) :: libhandle !< DLL handle
        character(len=*), intent(in)       :: s         !< Procedure name (Fortran string)
        integer, intent(in) :: required                 !< 0: info, 1: warning 2: error+stop

        ! Variables
        character(kind=C_CHAR,len=1024) :: tmp ! C-String
        
        
        ! remove the last null character, if there
        if ( s(len_trim(s):len_trim(s)) == C_NULL_CHAR ) then
          tmp = s(1:len_trim(s)-1)
        else
          tmp = trim(s)
        end if
        
        ! get procedure/function address
        loadsymbol1 = getprocaddress(libhandle,trim(tmp)//CHAR(0))


#ifdef __GFORTRAN__
        if (.not. C_ASSOCIATED(loadsymbol1)) then
#else
        if (loadsymbol1==0) then   
#endif
            if (required==0) then
                call log_info("Symbol not found in dll: "//trim(s))
            elseif(required==1) then
                call log_warning("Symbol not found in dll: "//trim(s))
            else
                call log_error("Symbol not found in dll: "//trim(s))
            endif

            
        endif
    end function


    !-----------------------------------------------------------------
    ! Overloaded "loadsymbol" without required argument (defaults to 1)
    !-----------------------------------------------------------------
#ifdef __GFORTRAN__
    TYPE(C_FUNPTR) function loadsymbol2(libhandle,s) 
#else
    integer(C_INTPTR_T) function loadsymbol2(libhandle,s)
#endif
        ! Arguments
        integer(C_INTPTR_T), intent(inout) :: libhandle !< Library handle
        character(len=*), intent(in)       :: s         !< Procedure name (Fortran string)
        loadsymbol2 = loadsymbol1(libhandle,s,1)
    end function



    !> Close a dll, returns true if all good
    logical function closedll(libhandle)
        integer(C_INTPTR_T), intent(inout) :: libhandle !< Library handle
        closedll = freelibrary(libhandle)
    end function

end module
