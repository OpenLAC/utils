@echo off

for /f %%i in ('git describe --tags --dirty --always') do set GIT_ID=%%i
for /f %%i in ('git describe --tags --abbrev^=0') do set GIT_VERSION=%%i 
for /f %%i in ('git branch --show-current') do set GIT_BRANCH=%%i


set S=#define GIT_ID "%GIT_ID%"
echo %S% > version.h


set GIT_VERSION=%GIT_VERSION: =%

echo %GIT_ID%
echo %GIT_VERSION%

IF not "%GIT_ID%"=="%GIT_VERSION%" (
    set GIT_VERSION=%GIT_VERSION%,-1
)


set S=#define GIT_VERSION %GIT_VERSION:.=,%
echo %S% >> version.h

set S=#define GIT_BRANCH "%GIT_BRANCH%"
echo %S% >> version.h


set S=#define USERNAME "%USERNAME%"
echo %S% >> version.h

set S=#define COMPUTERNAME "%COMPUTERNAME%"
echo %S% >> version.h

set S=#define BUILD_DATE "%DATE%"
echo %S% >> version.h

REM set S=#define BUILD_TIME "%TIME%"
REM echo %S% >> version.h

set S=#define BUILD_TYPE "%*"
echo %S% >> version.h


echo ----------------------
type version.h
echo ----------------------

 


