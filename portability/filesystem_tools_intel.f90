!> This module was created to avoid using ifport or dfport
! dfport was mainly used to call stat and system
!
module filesystem_tools
    use ifport
    ! TODO this can be made compiler/ platform independent
    ! TODO implement these: (in a gfortran compatible way)
    !splitpathqq,GETDRIVEDIRQQ, makedirqq, changedirqq, GETLASTERRORQQ splitpathqq
    implicit none
            
contains

    integer function mystat(filename,int_array)
        ! Arguments
        character(len=*),intent(in) :: filename  ! 
        integer, dimension(12), intent(out) :: int_array
        !
        logical :: exists
        !
        int_array=0
        INQUIRE(FILE=trim(filename), EXIST=exists) ! true if exist
        if (exists) then
            mystat=0 ! OK
        else
            mystat=2
        endif
    end function
            
    !> Returns true if a file or folder exist
    logical function file_exists(filename)
        character(len=*),intent(in) :: filename  ! 
        INQUIRE(FILE=trim(filename), EXIST=file_exists) ! true if exist
    end function file_exists

            
    !> Deletes a file
    subroutine delete_file(filename)
        character(len=*),intent(in) :: filename  ! 
        integer :: istatus
        !call system(DEL//trim(filename))
        istatus=unlink(filename) ! not standard
    end subroutine


	subroutine GetExePath(path)  
!DEC$ IF DEFINED(__linux__) !! preprocessor #ifdef gives error in cmake compilation order

    use ISO_C_BINDING
    use IFPORT

    implicit none

    interface
        function readlink(path, buf, bufsize) bind(C, NAME = 'readlink')
            import
            integer(C_SIZE_T) :: readlink
            character(KIND = C_CHAR), intent(IN) :: path(*)
            character(KIND = C_CHAR) :: buf(*)
            integer(C_SIZE_T), value :: bufsize
        end function
    end interface

    integer :: pid, i, idx
    integer(C_SIZE_T) :: szret
    character(256) :: path
    character(KIND = C_CHAR) :: cbuf(256)

    pid = GETPID()

    write (path, '(i0)') pid
    path = '/proc/'//TRIM(path)//'/exe'

    szret = readlink(TRIM(path)//C_NULL_CHAR, cbuf, SIZE(cbuf, KIND = C_SIZE_T))
    if (szret == -1) stop 'Error reading link'

    path = ''
    do i = 1, SIZE(cbuf)
        if (cbuf(i) == C_NULL_CHAR) exit
        path(i:i) = cbuf(i)
    enddo

    idx = INDEX(path, '/', BACK = .TRUE.)
	path(idx+1:) = ' '

!DEC$ ELSE	
	  USE DFWIN  
	  character(len=255) path          ! full name  
	  INTEGER       i, L                 ! length  
	  L = GetModuleFileName(NULL,path,LEN(path))   ! windows API  
	  i = index(path,"\", BACK = .TRUE.)
	  path(i+1:) = ' '
!DEC$ ENDIF
    end subroutine 
    
    function isabs(path)
        character(len=*), intent(in) :: path
        logical :: isabs
        isabs = .false.
!DEC$ IF DEFINED(__linux__) !! preprocessor #ifdef gives error in cmake compilation order
        if (path(1:1) == "/") isabs=.true.
!DEC$ ELSE	        
        if (path(2:2) == ":") isabs=.true.
!DEC$ ENDIF        
    end function
    

end module filesystem_tools
