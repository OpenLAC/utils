import sys
import os
import ctypes
from ctypes import c_char_p, c_long
import time
sys.path.append(os.path.dirname(__file__) + "/../..")
import subprocess
import shutil
from pathlib import Path
import re

# Avoid 
# urllib.error.URLError: <urlopen error [SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed: self signed certificate in certificate chain (_ssl.c:1124)>
# in some docker images
import ssl
try:
    _create_unverified_https_context = ssl._create_unverified_context
except AttributeError:
    # Legacy Python that doesn't verify HTTPS certificates by default
    pass
else:
    # Handle target environment that doesn't support HTTPS verification
    ssl._create_default_https_context = _create_unverified_https_context

def run(cmd, cwd='.', return_stdout=False):
    print("%s> %s" % (cwd, cmd), flush=True)
    cwd = Path(cwd).resolve()
    if cwd.is_file():
        cwd = cwd.parent

    stdout = subprocess.PIPE
    stderr = [subprocess.STDOUT, subprocess.PIPE][return_stdout]
    process = subprocess.Popen(cmd,
                               stdout=stdout,
                               stderr=stderr,
                               shell=True,
                               universal_newlines=True,
                               cwd=str(cwd))
    if return_stdout:
        stdout, stderr = process.communicate()
        output = stdout.strip() + "\n" + stderr.strip()
    else:
        output = ""
        while True:
            out = process.stdout.read(1)
            if isinstance(out, bytes):
                out = out.decode()
            if out == '' and process.poll() is not None:
                break
            if out != '':
                sys.stdout.write(out)
                sys.stdout.flush()
                output += out
        process.wait()

    if process.returncode != 0:
        raise RuntimeError("\nRunning command:\n%s> %s\nfailed:\n%s" % (cwd, cmd, output))
    return output


def clone(url, folder, depth="", branch='master'):
    folder = Path(folder)
    if depth:
        depth = "--depth %d" % depth
    if folder.is_dir():
        if (folder / ".git").is_dir():
            #run('git pull %s' % depth, folder)
            run('git fetch --all', cwd=folder)
            run(f'git checkout origin/{branch} -B {branch}', cwd=folder)
            run(f'git reset --hard origin/{branch}', cwd=folder)
            return
        else:
            shutil.rmtree(folder)
    run(f"git clone {depth} -b {branch} {url} {folder}")


def clone_pytesthawc2():
    if 'CI_PRIVATE_PAT' in os.environ:
        CI_PRIVATE_PAT = os.environ['CI_PRIVATE_PAT']
        clone("https://CIPrivateRunner:%s@gitlab.windenergy.dtu.dk/HAWC2/pytest_hawc2.git" % (CI_PRIVATE_PAT), 'pytest_hawc2')
    else:
        clone('git@gitlab.windenergy.dtu.dk:HAWC2/pytest_hawc2.git', 'pytest_hawc2')


def clone_hawc2binary(platform):
    p = hawc2bin_path(platform)
    if 'CI_PRIVATE_PAT' in os.environ:
        CI_PRIVATE_PAT = os.environ['CI_PRIVATE_PAT']
        clone("https://CIPrivateRunner:%s@gitlab.windenergy.dtu.dk/hawc2/hawc2-binary/%s.git" % (CI_PRIVATE_PAT, p), p)
        
        from urllib.request import Request, urlopen
        url = "http://gitlab.windenergy.dtu.dk/api/v4/projects/1203/jobs/artifacts/ci_runner/raw/"
               
        path, ext = {'win32':('build32/release/HAWC2License.dll?job=hawclicense_win32', '.dll'),
                     'x64':('build/release/HAWC2License_64.dll?job=hawclicense_win64', '_64.dll'),
                     'linux':('build/HAWC2License.so?job=hawclicense_linux', '.so')}[platform]
        req = Request(url + path)
        req.add_header('PRIVATE-TOKEN', CI_PRIVATE_PAT)
        
        with open(p/ f"HAWC2License{ext}", 'wb') as fid:
            fid.write(urlopen(req).read())
    else:
        clone("git@gitlab.windenergy.dtu.dk:HAWC2/hawc2-binary/%s.git" % p, p)
    #run('git submodule sync %s' % p.name, cwd=p.parent)
    #run('git submodule update --init %s' % p.name, cwd=p.parent)
    #run('git fetch --all', cwd=p)
    if platform == 'linux':
        run('git tag -d $(git tag)', cwd=p)  # delete local tags
        run('git fetch --tags', cwd=p) 
    else:
        run('git fetch origin --prune --tags "+refs/tags/*:refs/tags/*"', cwd=p)  # delete local tags
    #run('git checkout master', cwd=p)
    #run('git reset --hard origin/master', cwd=p)
    return Path("./%s/" % p)


def hawc2bin_path(platform):
    return Path('hawc2-%s' % platform.replace("x64", 'win64'))


def get_file_version(filename):
    try:
        import win32api
        lang, codepage = win32api.GetFileVersionInfo(str(filename), '\\VarFileInfo\\Translation')[0]
        # any other must be of the form \StringfileInfo\%04X%04X\parm_name, middle
        # two are language/codepage pair returned from above
        strInfoPath = u'\\StringFileInfo\\%04X%04X\\%%s' % (lang, codepage)
        return "%s (%s)"%(win32api.GetFileVersionInfo(str(filename), strInfoPath%'FileVersion'),
                          win32api.GetFileVersionInfo(str(filename), strInfoPath%'ProductVersion'))
    except:
        try:
            dll = ctypes.CDLL(str(filename))
            f = getattr(dll, 'get_file_version')
            f.argtypes = [c_char_p, c_long]
            s = "".ljust(255)
            arg = c_char_p(s.encode('utf-8'))
            f(arg, len(s))
            return arg.value.decode().strip()
        except Exception:
            return 'Unknown'


def is_tagged_version(path):
    try:
        tag = run("git describe --tags --abbrev=0 --dirty", path, return_stdout=True).split("\n")[0].strip()
    except RuntimeError as e:
        if "No names found, cannot describe anything" in str(e):
            raise RuntimeError("No tags found in repository. Please addd one e.g: 0.1")
        raise
    description = run("git describe --tags --always", path, return_stdout=True).strip()
    return tag == description


def get_current_git_tag(path):
    if path.is_file():
        path = path.parent
    return run("git describe --tags --abbrev=0", path, return_stdout=True).split("\n")[0].strip()
    # return run("git tag --sort=-creatordate", path, return_stdout=True).split("\n")[0].strip()


def get_module_info(module_file):
    tag = get_current_git_tag(module_file)
    message = run("git tag -l -n99 %s" % tag, module_file, return_stdout=True).replace(tag, "").strip()
    date = run('git log -1 --format=%%ai --tags %s' % tag, module_file, return_stdout=True)[:10].strip()
    name = run('git log -1 --format="%%an" --tags %s' % tag, module_file, return_stdout=True).strip()
    return tag, date, name, message


def get_next_version(module_file, h2bin_path):
    module = os.path.splitext(os.path.basename(module_file))[0]
    if module.lower().startswith('hawc2mb'):
        h2_version = get_current_git_tag(module_file)
        return h2_version + ".0"
    else:
        current_bin_tag = get_current_git_tag(h2bin_path)
        print('Current binary tag:', current_bin_tag)
        next_bin_tag = ".".join([str(int(v)+v_inc) for v, v_inc in zip(current_bin_tag.split("."), [0, 0, 0, 1])])
        print('New bindary tag:', next_bin_tag)
        return next_bin_tag


def update_contents(h2bin_path, filename, version):
    s = "%-30s\t%s\n" % (os.path.basename(filename), version)
    contents_path = h2bin_path / "Contents.txt"
    lines = (contents_path).read_text().splitlines(keepends=True)
    for i, l in enumerate(lines):
        if l.lower().startswith(os.path.basename(filename).lower()):
            lines[i] = s
            break
    else:
        lines.append(s)
    with open(contents_path, 'w', newline='\n') as fid:
        fid.write("".join(lines).replace("\r", ""))


def update_changelog(h2bin_path, new_bin_tag, module_file, module_tag, date, module_tag_message):
    module = module_file.stem
    info = "  ".join(["%-12s" % new_bin_tag, date, "%s(%s)" % (module, module_tag)])
    message = ("\n" + " " * 60).join([l.strip() for l in module_tag_message.split("\n")])
    with open(h2bin_path / 'Changelog.txt') as fid:
        change_log = fid.read().replace("\r", "").strip()
    change_log += ("\n%-60s%s\n" % (info[:58], message))
    with open(h2bin_path / 'Changelog.txt', 'w', newline='\n') as fid:
        fid.write(change_log)


def update_file(platform, module_files, tagged_version):
    h2bin_path = clone_hawc2binary(platform)
    h2bin_path = Path("./%s/" % hawc2bin_path(platform))
    next_version = get_next_version(module_files[0], h2bin_path)
    commit_msg = []
    for module_file in module_files:
        module_file = Path(module_file)
        module_file_version = get_file_version(module_file)
        
        module_tag, module_tag_date, _, module_tag_message = get_module_info(module_file)
        module_description = run('git describe --tags --always', module_file.parent, return_stdout=True)
        if module_file_version == 'Unknown':
            module_file_version = module_description
        current_bin_tag = get_current_git_tag(h2bin_path)
        print ("update_file:, module_file, tagged_version, module_file_version, next_version, module_tag, current_bin_tag")
        print ("update_file:", module_file, tagged_version, module_file_version, next_version.strip(), module_tag, current_bin_tag)
        if module_file.suffix==".exe":
            os.chmod(module_file, 0o770)
        shutil.copyfile(module_file, h2bin_path / module_file.name)
        if module_file.suffix==".exe":
            os.chmod(h2bin_path / module_file.name, 0o770)

        update_contents(h2bin_path, module_file, module_file_version)
        update_changelog(h2bin_path, next_version, module_file, module_tag, module_tag_date, module_tag_message)
        commit_msg.append("%s(%s)" % (module_file.name, module_file_version))
    run('git add %s Contents.txt Changelog.txt' % (" ".join([f'"{f.name}"' for f in module_files])), h2bin_path)
    
    
    run('git commit -m "%s"' % (", ".join(commit_msg)), h2bin_path)
    if tagged_version:
        run('git tag "%s"' % next_version, h2bin_path)


def push(platform):
    h2bin_path = hawc2bin_path(platform)
    run('git push', h2bin_path)
    run('git push --tags', h2bin_path)


def run_tests(platform, configuration, module_file, verbose=False):
    if module_file:
        module_name = module_file.stem
        if module_name.endswith("_64"):
            module_name = module_name[:-3]
        if module_name.startswith('lib'):
            module_name = module_name[3:]
        test_file = "dlls/test_%s.py" % module_name
    else:
        test_file = ""
    if str(module_file).endswith("HAWC2MB.exe") or Path('file_info.h').exists() and Path('file_info.h').read_text().startswith('#define FILE_NAME "HAWC2MB"'):
        test_file = ""
    else:
        print("Run local tests")
        args = ['tests']
        if verbose:
            args.append('-x')
        import pytest
        res = pytest.main(args)
        if res:
            if str(res) != "ExitCode.USAGE_ERROR":
                print(str(res))
                exit(res)

        #if configuration.endswith('codecov'):
        #    code_cov()
        
        # HAWC2 has pytest_hawc2 as a submodule, so only clone if not hawc2mb
    if not os.path.isdir('pytest_hawc2'):
        print("Clone pytest_hawc2 and run module tests")
        clone_pytesthawc2()
    run('"%s" run_pytest.py "%s" "../%s/HAWC2MB.exe"' %
        (sys.executable, test_file, hawc2bin_path(platform)), "pytest_hawc2")

    if configuration.endswith('codecov'):
        code_cov()


def code_cov():
    # find folder containing PGOPTI.SPI file.
    # this folder contains the *.dyn files but their presence may be slightly delayed
    p = list(Path().glob("**/PGOPTI.SPI"))
    if p:
        p = p[0].parent
        time.sleep(1)
        run("profmerge.exe", p)
        if (p/'pgopti.dpi').exists():
            run("codecov", p)
            with open(p / "CodeCoverage/__CODE_COVERAGE.HTML") as fid:
                print("Total coverage: ", fid.readlines()[-9].replace('<TD ALIGN="center" STYLE="font-weight:bold"> ', "").replace("</TD>", "%"))
        else:
            print ("pgopti.dpi not found in ", str(p))
    else:
        print("PGOPTI.SPI not found")

def compile_sln(sln, platform, configuration, rebuild=True):
    assert platform in ['win32', 'x64']
    run('devenv %s /%s "%s|%s"' % (sln, ['build', 'rebuild'][rebuild], configuration, platform))


def cmake(platform, configuration, args=[], builder=None, compiler=None, rebuild=True):
    """

    Parameters
    ----------
    platform : {'win32', 'win64'}, None
        Platform (Only for Visual Studio >= 8)
    """
    if ('CI_PROJECT_URL' in os.environ and os.environ['CI_PROJECT_URL']=="https://gitlab.windenergy.dtu.dk/HAWC2/HAWC2" and
            'CI_PRIVATE_PAT' in os.environ):
        clone(url=f"https://CIPrivateRunner:{os.environ['CI_PRIVATE_PAT']}@gitlab.windenergy.dtu.dk/hawc2private/encr_lib.git", folder='ENCR_LIB_PRIVATE', depth=1)
    project_folder = Path('.')
    if platform == 'win32':
        build_folder = project_folder / 'build32'
    else:
        build_folder = project_folder / 'build'
    build_folder.mkdir(exist_ok=True)

    cmake_txt = (project_folder / "CMakeLists.txt").read_text()
    project_name = re.search(r'project\((\S+)', cmake_txt)[1]
    build_type = re.search(r'set *\(BUILD_TYPE +(EXE|SHARED|STATIC) *\)', cmake_txt)[1]

    if configuration.endswith('codecov'):
        configuration = configuration[:-7]
        args.append('-DCODECOV="/Qcov-gen"')
    if configuration.endswith('cluster'):
        configuration = configuration[:-7]
        args.append('-DCLUSTER=1')

    if builder is None:
        if 'linux' in platform.lower():
            run("bash %s %s %s" % (Path(__file__).parent.parent / 'version/version.sh', platform, configuration))
        else:
            run(str(Path(__file__).parent.parent / 'version/version.bat') + " %s %s" % (platform, configuration))
        for f in ["file_info.h", 'version.h'][1:]:
            if os.path.isfile(f):
                shutil.copy(f, Path(__file__).parent.parent / 'version')

        if platform == 'x64':
            # visual studio builder
            args.append("-Ax64")
        elif platform == 'win32':
            args.append("-Awin32")
        elif 'linux' in platform.lower():
            args.append('-DCMAKE_BUILD_TYPE=%s' % configuration.upper())
         
   
    if builder is not None:
        if builder == 'mingw32-make':
            args.append('-D CMAKE_MAKE_PROGRAM="%s"' % builder)
            args.append('-G "MinGW Makefiles"')
        elif builder == 'ninja':
            args.append('-GNinja')
            args.append('-DCMAKE_BUILD_TYPE=%s' % configuration.upper())
        else:
            raise NotImplementedError(builder)
    if compiler is not None:
        args.append('-DCMAKE_Fortran_COMPILER="%s"' % compiler)
    elif "Fortran" in (project_folder / "CMakeLists.txt").read_text():
        args.append('-DCMAKE_Fortran_COMPILER=ifort')


    print('Running cmake')
    print(run('cmake .. %s' % " ".join(args), build_folder))
    print("Compiling")

    if builder is None:
        if platform in ['win32', 'x64']:
            sln = list(build_folder.glob("*.sln"))[0]
            compile_sln(sln, platform, configuration, rebuild)
            return list((build_folder / configuration).glob("*.exe")) + list((build_folder / configuration).glob("*.dll"))
        else:  # platform=='linux':
            run('make -j %s' % (['', '-B'][rebuild]), cwd=build_folder)
            for dll in list((build_folder).glob("lib*.so")):
                dll.rename(dll.parent / str(dll.name)[3:])
            return list((build_folder).glob("*.exe")) + list((build_folder).glob("*.so"))
    elif builder == 'mingw32-make':
        print(run("%s %s %s" % (str((project_folder / 'utils/version/version.bat').absolute()), platform, configuration), project_folder))
        shutil.copy(project_folder / 'version.h', project_folder / 'utils/version')
    #    shutil.copy(project_folder / 'file_info.h', project_folder / 'utils/version')
        run('mingw32-make %s' % (['', '-B'][rebuild]), cwd=build_folder)
        return list((build_folder).glob("*.dll"))
    elif builder == 'ninja':
#        print(run("%s %s %s" % (str((project_folder / 'utils/version/version.bat').absolute()), platform, configuration), project_folder))
#        shutil.copy(project_folder / 'version.h', project_folder / 'utils/version')
#        shutil.copy(project_folder / 'file_info.h', project_folder / 'utils/version')
        run('ninja', cwd=build_folder)
        return list((build_folder).glob("*.exe")) + list((build_folder).glob("*.dll"))
    else:
        raise NotImplementedError(builder)

    print("Done")


def get_platform_configuration(platform_conf):

    platform, configuration, *_ = platform_conf.lower().split("|") + ["release"]
    platform = platform.replace("win64", 'x64')
    tagged_version = is_tagged_version(".")

    assert platform in ['win32', 'x64', 'linux'], platform
    assert configuration in ['release', 'debug', 'debugcodecov', 'releasecodecov', 'debugcluster', 'releasecluster', 'debugcodecovcluster', 'releasecodecovcluster'], configuration
    return platform, configuration, tagged_version


def get_platform_configuration_from_argv():
    return get_platform_configuration(sys.argv[1])


def main(argv):
    try:
        platform, configuration, tagged_version = get_platform_configuration(argv[1])
        cmd = argv[2]
        if len(argv) > 3:
            args = argv[3:]
        else:
            args = []
        print("platform:", platform)

        print("Configuration:", configuration)
        print("Command: %s(%s)" % (cmd, args))
    except (IndexError, AssertionError) as e:
        raise type(e)(str(e) + info).with_traceback(sys.exc_info()[2])
    cmake_kwargs = {}
    if 'build' in args:
        args.remove('build')
        cmake_kwargs['rebuild'] = False
    else:
        cmake_kwargs['rebuild'] = True
        if 'rebuild' in args:
            args.remove('rebuild')
    if 'gfortran' in args:
        args.remove('gfortran')
        cmake_kwargs['compiler'] = 'gfortran'
        if platform in ['win32', 'x64']:
            cmake_kwargs['builder'] = 'mingw32-make'
        

    if 'ninja' in args:
        args.remove('ninja')
        cmake_kwargs['builder'] = 'ninja'

    if cmd == 'compile_sln':
        sln = args[0]
        compile_sln(sln, platform, configuration)
    elif cmd == 'cmake':
        cmake(platform, configuration, args, **cmake_kwargs)
    elif cmd == 'update':
        module_path = Path(args[0])
        if module_path.is_dir():
            f_lst = [f for ext in ['.exe','.dll','.so'] for f in module_path.iterdir() if f.suffix.lower()==ext]
            update_file(platform, f_lst, tagged_version)
        else:
            update_file(platform, module_path, tagged_version)
    elif cmd == 'test':
        if args:
            module_file = Path(args[0])
        else:
            module_file = None
        run_tests(platform, configuration, module_file, verbose=True)
    elif cmd == 'cmake_update':
        module_file = cmake(platform, configuration, args, **cmake_kwargs)[0]
        update_file(platform, [module_file], tagged_version)
    elif cmd == 'cmake_update_test':
        module_file = cmake(platform, configuration, args, **cmake_kwargs)[0]
        update_file(platform, [module_file], tagged_version)
        sep = "#"*50
        print ("\n%s\nTesting\npython utils/ci/ci_utils.py %s test %s\n%s\n"%(sep,platform,module_file,sep))
        run_tests(platform, configuration, module_file)
    elif cmd == 'code_cov':
        code_cov()
    elif cmd == 'push':
        push(platform)
    elif cmd == 'push_if_tagged':
        if tagged_version:
            push(platform)


if __name__ == '__main__':
    info = """
    usage:
    python ci_utils.py <platform>[|<configuration>] <cmd> <args>

    platform: {win32, x64, linux}
    configuration: {debug, release(default)}
    cmd:
    - compile_sln <sln>
    - cmake
    """
    main(sys.argv)

