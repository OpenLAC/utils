import shutil
if __name__ == "__main__":
    import os

    with open("CodeCoverage/__CODE_COVERAGE.HTML") as fid:
        print("Total coverage: ", fid.readlines()[-9].replace('<TD ALIGN="center" STYLE="font-weight:bold"> ', "").replace("</TD>", "%"))
