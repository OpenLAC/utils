
module logging
#include "file_info.h"  
! Module used to write to log file from both the main program and dlls.
! In the main program, the preprocessor definition "MAIN" must be defined 
    
! Open and close the log file from the main program via 
! open_log(<log_filename>) and close_log
!    
! Write to the log via:
! log_info(msg), log_warning(msg) and log_error(msg)
!
! Overloaded proceedures for different combinations of variables are implemented, e.g.:
! log_xxx(c, i), log_xxx(c,i,c), log_xxx(c,d) where c=character(*), i=integer, d=read*8
    
    
use iso_c_binding, only: C_PTR, C_INTPTR_T, C_BOOL, C_CHAR, C_NULL_FUNPTR, C_F_PROCPOINTER, &
                         C_ASSOCIATED
use StringUtils, only: fortranstring2c, cstring2fortran
! dfwin module defines the Windows API or Linux API for calling a dynamic library
! It has been wrapped for both Intel fortran compiler and gfortran compiler
use dfwin, only: getprocaddress, loadlibrary
use iso_fortran_env, only: error_unit

implicit none
#ifdef MAIN
character(50) :: dll_name = ''
#else
character(50) :: dll_name = FILE_NAME
#endif

character(kind=c_char, len=1), dimension(50), save :: c_dll_name=''
integer, save :: logfile_unit

    
interface log_info
    module procedure log_info_c, log_info_ci, log_info_cic, log_info_cici, log_info_cd
end interface
interface log_warning
    module procedure log_warning_c, log_warning_ci, log_warning_cic, log_warning_cici, log_warning_cd
end interface
interface log_error
    module procedure log_error_c, log_error_ci, log_error_cic, log_error_cici, log_error_cd
end interface

contains
    
subroutine open_log(log_filename, unit)
    ! Note: folder of log_filename must exists

    use BuildInfo, only: BuildInfo_echo
    character(len=*), intent(in):: log_filename
    integer, intent(in), optional  :: unit
    character*8          :: datstr
    character*10         :: timstr
    character*5          :: timzon
    integer,dimension(8) :: values
    character*(*) :: seplin
    parameter (seplin='________________________________________________________________________')

    
    if (present(unit)) then
        logfile_unit=unit
    else
        logfile_unit=error_unit
    endif
    
    open (unit = logfile_unit, err=10, file = trim(log_filename))
    write (0, *) 'Logfile: ' // trim(log_filename) //' is open for log outputs'
    call buildinfo_echo(fin=logfile_unit)
    CALL DATE_AND_TIME(DATSTR,TIMSTR,TIMZON,VALUES)
    write (logfile_unit,fmt='(A)') seplin
    write (logfile_unit,fmt='(2X,A)')  'Log file output'
    write (logfile_unit,fmt='(50X,A20)') 'Time : '//timstr(1:2)//':'//timstr(3:4)//':'//timstr(5:6)
    write (logfile_unit,fmt='(52X,A20)') 'Date : '//datstr(7:8)//':'//datstr(5:6)//'.'//datstr(1:4)
    write (logfile_unit,fmt='(A)') seplin
    return
    10 call log_warning('Error opening logfile:'//trim(log_filename))
end subroutine
    

subroutine write_log(msg, error, warning)
    character(len=*) :: msg
    logical, intent(in), optional :: error, warning
    logical :: e,w
   

    e=.false.
    w=.false.
    if (present(error)) e = error 
    if (present(warning)) w=warning
    
    if (trim(dll_name)/='') then
        ! write to log file via extern_write_log (from dll)
        call dll_write_log(msg,e,w)
    else
        ! write directly to log (from main program or from dll if extern_write_log is not found)
        call main_write_log(msg,e,w,dll_name)
    endif
end subroutine

subroutine main_write_log(msg, error, warning, dll_name)
    character(len=*) :: msg
    logical, intent(in) :: error, warning
    character(len=50), intent(in) :: dll_name
    character(len=len(dll_name)+2) :: dn
    integer :: n
    
    if (trim(dll_name)/='') then
        dn=trim(dll_name)//': '
        n = len(trim(dll_name)) + 2
    else
        dn=''
        n = 0
    endif
        
    if (error) then 
        write(logfile_unit,'(A)') dn(1:n)//'*** ERROR *** '//msg
        stop (1)
    elseif (warning) then 
        write(logfile_unit,'(A)') dn(1:n)//'*** WARNING *** '//msg
    else
        write(logfile_unit,'(A)') dn(1:n)// msg
    endif
end subroutine

!---------------------------------------------------------------------------
subroutine dll_write_log(msg, error, warning)
    character(len=*) :: msg
    character(kind=c_char, len=1), dimension(:), allocatable :: c_msg
    logical, intent(in) :: error, warning
    logical(kind=c_bool) :: e,w    
    
    ! stuff for calling extern_write_log in main program    

    integer(C_INTPTR_T), save   :: pdll
    logical, save               :: bFirstCall = .TRUE.

    Interface
        subroutine extern_write_log(c_msg, n, dll_name, error, warning) bind(C, name="extern_write_log")
            use iso_c_binding, only:c_char, c_bool
            integer, intent(in) :: n
            character(kind=c_char, len=1), intent(in) :: c_msg(n)
            character(kind=c_char), intent(in) :: dll_name(50)
            logical(kind=c_bool), intent(in), optional :: error, warning
        end subroutine
    end interface
    !pointer(pfunc,extern_write_log)
    PROCEDURE(extern_write_log), BIND(C), POINTER :: pfunc
    ! Initialise
    if (bFirstCall) then
      bFirstCall = .FALSE.
      pdll = loadlibrary(CHAR(0))

      if (pdll.ne.0) then 
          ! in dll
          call fortranstring2c(dll_name, c_dll_name)          
          
          !pfunc = getprocaddress(pdll,'extern_write_log'//CHAR(0))

          ! getprocaddress returns an integer(c_intptr_t) in intel fortran
          call C_F_PROCPOINTER(TRANSFER(getprocaddress(pdll,'extern_write_log'//CHAR(0)), C_NULL_FUNPTR), pfunc)
          !pfunc = c_loc(TRANSFER(getprocaddress(pdll,'extern_write_log'//CHAR(0)), C_NULL_FUNPTR))
          ! alternatively using cray pointer
          ! pointer(pfunc, func)
          ! pfunc = getprocaddress(pdll,'func'//CHAR(0))
 
          if (.NOT. ASSOCIATED(pfunc)) print *, '*** WARNING *** extern_write_log not found in main program'
      else
          ! in main program
          nullify(pfunc)
      endif
    endif
    
    ! write to log file via extern_write_log
    e=error
    w=warning
    if (associated(pfunc)) then 
        allocate(c_msg(len(msg)))
        call fortranstring2c(msg, c_msg)
        call extern_write_log(c_msg, len(msg),c_dll_name, e,w)
        deallocate(c_msg)
    else
        call main_write_log(msg, error, warning,dll_name)
    end if
end subroutine

!---------------------------------------------------------------------------
subroutine extern_write_log(c_msg, n_msg, dll_name, error, warning) bind(C, name="extern_write_log")
    ! Allow dlls to write to log file opened in main program
    !DEC$ ATTRIBUTES DLLExport :: extern_write_log
    use iso_c_binding, only:c_char, c_bool
    use StringUtils, only: cstring2fortran
    character(kind=c_char, len=1), intent(in) :: c_msg(n_msg)
    integer, intent(in) :: n_msg
    character(kind=c_char), intent(in) :: dll_name(50)
    character(len=50) :: n
    logical(kind=c_bool), intent(in), optional :: error, warning
    logical :: e,w

    character(len=n_msg) :: msg
    call cstring2fortran(c_msg,msg)
    e = error
    w = warning
    call cstring2fortran(dll_name, n)

    call main_write_log(msg, e, w, n)

end subroutine

subroutine close_log()
    close(logfile_unit)
end subroutine




!************************************************************************
! Message conversion functions
!************************************************************************
function ci(c,i) result(msg)
    character(*) :: c
    character(len=:), allocatable :: msg
    integer :: i
    character(len=100)::c_i
    write(c_i,*) i
    msg = c //' '// trim(ADJUSTL(c_i))
end function
    
function cd(c,d) result(msg)
    character(*) :: c
    character(len=:), allocatable :: msg
    real*8 :: d
    character(len=100)::c_d
    write(c_d,*) d
    msg = c //' '// trim(ADJUSTL(c_d))
end function
    
function cic(c1,i,c2) result(msg)
    character(*) :: c1,c2
    character(len=:), allocatable :: msg
    integer :: i
    character(100)::c_i
    write(c_i,*) i
    msg = c1 //' '// trim(ADJUSTL(c_i))//' '//c2
end function
    
function cici(c1,i1,c2,i2) result(msg)
    character(*) :: c1,c2
    character(len=:), allocatable :: msg
    integer :: i1,i2
    character(100)::c_i1,c_i2
    write(c_i1,*) i1
    write(c_i2,*) i2
    msg = c1 //' '// trim(ADJUSTL(c_i1))//' '//c2//' '// trim(ADJUSTL(c_i2))
end function
!************************************************************************
! Log Info routines
!***********************************************************************
subroutine log_info_c(c)
    character(*) :: c
    call write_log(c,.false.,.false.)
end subroutine
    
subroutine log_info_ci(c, i)
    character(*) :: c
    integer :: i
    call write_log(ci(c,i),.False.,.false.)
end subroutine
    
subroutine log_info_cd(c, d)
    character(*) :: c
    real*8 :: d
    call write_log(cd(c,d),.False.,.false.)
end subroutine
    
subroutine log_info_cic(c1, i,c2)
    character(*) :: c1,c2
    integer :: i
    call write_log(cic(c1,i,c2),.False.,.false.)
end subroutine
    
subroutine log_info_cici(c1, i1,c2,i2)
    character(*) :: c1,c2
    integer :: i1,i2
    call write_log(cici(c1,i1,c2,i2),.False.,.false.)
end subroutine
    
    
!************************************************************************
! Log Warning routines
!************************************************************************
subroutine log_warning_c(c)
    character(*) :: c
    call write_log(c,.false.,.true.)
end subroutine
    
subroutine log_warning_ci(c, i)
    character(*) :: c
    integer :: i
    call write_log(ci(c,i),.False.,.true.)
end subroutine
    
subroutine log_warning_cd(c, d)
    character(*) :: c
    real*8 :: d
    call write_log(cd(c,d),.False.,.true.)
end subroutine
    
subroutine log_warning_cic(c1, i,c2)
    character(*) :: c1,c2
    integer :: i
    call write_log(cic(c1,i,c2),.False.,.true.)
end subroutine
    
subroutine log_warning_cici(c1, i1,c2,i2)
    character(*) :: c1,c2
    integer :: i1,i2
    call write_log(cici(c1,i1,c2,i2),.False.,.true.)
end subroutine

!************************************************************************
! Log Error routines
!************************************************************************
subroutine log_error_c(c)
    character(*) :: c
    call write_log(c,.true.,.false.)
end subroutine
    
subroutine log_error_ci(c, i)
    character(*) :: c
    integer :: i
    call write_log(ci(c,i),.true.,.false.)
end subroutine
    
subroutine log_error_cd(c, d)
    character(*) :: c
    real*8 :: d
    call write_log(cd(c,d),.true.,.false.)
end subroutine
    
subroutine log_error_cic(c1, i,c2)
    character(*) :: c1,c2
    integer :: i
    call write_log(cic(c1,i,c2),.true.,.false.)
end subroutine

subroutine log_error_cici(c1, i1,c2,i2)
    character(*) :: c1,c2
    integer :: i1,i2
    call write_log(cici(c1,i1,c2,i2),.true.,.false.)
end subroutine
end module
