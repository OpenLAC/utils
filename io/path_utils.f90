module path_utils
! get_path and create_dirs copied from genout_tools2
implicit none
public :: create_dirs

contains

!--------------------------------------------------------------------
!--------------------------------------------------------------------
function get_path(file)
  use filesystem_tools, only: splitpathqq
  character(len=*), intent(in)	:: file
  character(256)				:: get_path

  INTEGER(4) length
  CHARACTER(256)      drive
  CHARACTER(256)      name1,path
  CHARACTER(256)      ext
  
  length = splitpathqq(file, drive, path, name1, ext)
  get_path=path
end function

!--------------------------------------------------------------------
!--------------------------------------------------------------------

subroutine create_dirs(path)
  use filesystem_tools, only: splitpathqq,GETDRIVEDIRQQ, makedirqq, changedirqq, GETLASTERRORQQ
  use logging
  character(len=*), intent(in) :: path

  CHARACTER(256) d0,d1,d2,p1
  INTEGER(4) length,istatus,istatus1
  CHARACTER(256)      drive
  CHARACTER(256)      dir0,dir1,dir2,dir3,dir4,drivedir,str
  CHARACTER(256)      name0,name1,name2
  CHARACTER(256)      ext
  INTEGER(4)          result  ! TODO: it is not a good practice to use 'result' as a variable because
                              !       it is a keywords
  integer             q,q2,len1

  ! current directory before changing anything
  length=GETDRIVEDIRQQ (d0) 
  length = splitpathqq(d0, drive, dir0, name0, ext)
  dir0=trim(dir0)//trim(name0)//trim(ext)

  p1=path
  q2=index(p1,'..',.true.)
  do while (q2.gt.0)
    q=index(dir0,'\\',.true.)
    result = changedirqq(dir0(1:q-1))
    p1=path(q2+3:len(path))  
    q2=index(p1,'..',.true.)
  enddo 

  ! current directory after moving the common basis
  length=GETDRIVEDIRQQ (d1) 
  length = splitpathqq(d1, drive, dir1, name1, ext)
  dir1=trim(dir1)//trim(name1)//trim(ext)

  ! requested directory
  d2 = trim(p1)
  length = splitpathqq(d2, drive, dir2, name2, ext)

  dir3=dir2(len_trim(dir1)+2:len_trim(dir2)-1)

  q=index(dir3,'\\')
  do while (q.gt.0)
    dir4=dir3(1:q-1)
    istatus=0
    call log_info ('Creating file folder '//trim(dir3(1:q)))
    result = MAKEDIRQQ(dir3(1:q))
    istatus= GETLASTERRORQQ( )
    if (istatus.eq.22) then
      str=''
      len1=len_trim(dir4)
      str(1:len1)=dir4(1:len1-1)
      result = MAKEDIRQQ(trim(str))
      istatus1= changedirqq(str)
      istatus= GETLASTERRORQQ( )
    else
      istatus1= changedirqq(dir3(1:q))
    endif
    if (istatus1.eq.0) then
      call log_error('Not able to create or enter subfolder: '//trim(trim(drivedir)//'\\'//dir3(1:q)))
    else
	  length = GETDRIVEDIRQQ (drivedir)
	
	  dir3=dir3(q+1:len_trim(dir3))
	  q=index(dir3,'\\')
	 endif
  enddo
  dir4=dir3  
  result = MAKEDIRQQ(dir4)
  istatus = CHANGEDIRQQ(dir0)
end subroutine create_dirs

function fix_path(prefix_path, path)
    use StringUtils
    character(len=*), intent(in) :: prefix_path, path
    character*1024 :: fix_path, tmp, prefix_tmp    
    integer :: i
    prefix_tmp = replace(prefix_path, "\\","/")
    i = len_trim(prefix_tmp)
    if (i>0) then 
      if (prefix_tmp(i:i)/="/") prefix_tmp(i+1:i+1) = "/"
    endif
    tmp = replace(path, "\\","/")
#ifdef __linux__
    fix_path = get_case_sensitive_path(prefix_tmp, tmp)
#else
    fix_path = trim(prefix_tmp) // tmp
#endif
end function


function get_case_sensitive_path(prefix_path, path)
  use filesystem_tools, only: file_exists, isabs
  use StringUtils
  use logging
  character(len=*) :: prefix_path, path
  character*1024 :: get_case_sensitive_path
  integer :: i, IERR
  
  integer :: n
  character(len=256), allocatable, dimension(:) :: str_arr
  character(len=256) :: tmp_str 
  ! print *, "get_case_sensitive_path("//trim(prefix_path) // ", " // trim(path)//")"
  if (file_exists(trim(prefix_path) // trim(path))) then 
    get_case_sensitive_path = ''
    get_case_sensitive_path = trim(prefix_path) // trim(path)
  else
    
    n = count_char(path, "/")
    str_arr(:) = ''
    str_arr = split(path,"/", n)
    i = len_trim(prefix_path)
    if (i>0) then
	if (prefix_path(i:i)=="/") then
            get_case_sensitive_path = prefix_path(1:i-1)
	else
	    get_case_sensitive_path = prefix_path
        endif 
    elseif (isabs(trim(path))) then
        get_case_sensitive_path = ""
    else
        get_case_sensitive_path = "."
    endif
    IERR=0 
    do i=1,n
        if (trim(str_arr(i))/='.') then
            call get_case_sensitive_name(get_case_sensitive_path, str_arr(i), .True., tmp_str, IERR)
            get_case_sensitive_path = trim(tmp_str)
	    if (IERR/=0) exit
        endif
    end do
    if (IERR==0) then
      call get_case_sensitive_name(get_case_sensitive_path, str_arr(n+1), .False., tmp_str, IERR)
      get_case_sensitive_path =  trim(tmp_str)
      if (file_exists(trim(get_case_sensitive_path)) .and. trim(get_case_sensitive_path)/=trim(prefix_path)//trim(path) .and. trim(get_case_sensitive_path)/="./"//trim(prefix_path)//trim(path)) then 
          if (get_case_sensitive_path(1:len_trim(prefix_path))==trim(prefix_path)) then
              i=len_trim(prefix_path)+1 ! remove prefix
          else
              i=1
          endif
          call log_warning("Used 'find' iteratively to find '"//trim(path)//"' in '"//trim(prefix_path)//"'"//NEW_LINE('a')//"Correct (case sensitive) filename is '"//trim(get_case_sensitive_path(i:))//"'. Please fix the input to avoid this warning and the potential resource demanding use of find")
      endif
    else
      get_case_sensitive_path = trim(prefix_path) // trim(path)
    endif
  endif
  ! print *, "get_case_sensitive_path, result: " // trim(get_case_sensitive_path)
end function

subroutine get_case_sensitive_name(path, name, dir, case_sensitive_name, ierr)
#ifdef __linux__
    USE IFPORT
    USE logging
    integer :: unit
    character*256 :: tmp_filename, tmp_name
    INTEGER(4) pid
    character(len=1) :: type_arg
#endif
    integer, intent(out) :: IERR
    character*256, intent(out) :: case_sensitive_name
    character(len=*), intent(in) :: path, name
    logical :: dir
    ! print *, "get_case_sensitive_name(path="//trim(path)//", name="//trim(name)//")"
    case_sensitive_name = ''
    ierr = 0
    if (trim(path)=='' .and. trim(name)=='') then 
        case_sensitive_name = '/'
    elseif(trim(name)=='.') then
        case_sensitive_name = trim(path)
    elseif(trim(name)=='..') then
        case_sensitive_name = trim(path) // "/" // trim(name)
    else
        case_sensitive_name = trim(path) // "/" // trim(name)
#ifdef __linux__
        if (dir) then
            type_arg = 'd'
        else
            type_arg = 'f'
        endif
        
        ! make tmp name
        tmp_filename = ''
        pid = GETPID()
        write (tmp_filename, 101) 'tmp',pid
101 format (A,Z8.8,'.tmp')

        call log_info("find "//trim(path)//" -maxdepth 1 -type "//type_arg//" -ipath '*"//trim(name)//"'")
        IERR = system("find "//trim(path)//" -maxdepth 1 -type "//type_arg//" -ipath '*"//trim(name)//"' > " // trim(tmp_filename))
        open(newunit=unit,file=tmp_filename,STATUS='OLD')
        tmp_name = ''
        read(unit,'(A)',iostat=IERR) tmp_name
        close(unit, status='delete')
        if ((IERR==0).and.(path(1:1)/=char(0))) then
            case_sensitive_name = ''
            case_sensitive_name = trim(tmp_name)
        endif
#endif
    endif
    ! print *, "IERR", ierr, "result: ", trim(case_sensitive_name)
end subroutine


end module
