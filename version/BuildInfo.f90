MODULE BuildInfo

IMPLICIT NONE
#include "version.h"
#include "file_info.h"

type tbuildinfo
    character*255   :: git_tag = GIT_ID
    character*255   :: git_branch = GIT_BRANCH
    character*255   :: builder = USERNAME  
    character*255   :: computer_name = COMPUTERNAME
    character*255   :: build_date = BUILD_DATE
    !character*255   :: build_time = BUILD_TIME
    character*255   :: build_type = BUILD_TYPE
end type 
!
CONTAINS

SUBROUTINE BuildInfo_echo(bin,fin)
    type (tbuildinfo), optional   :: bin
    type (tbuildinfo) :: b
    integer, optional   :: fin
    integer             :: fid
    !
    if (present(fin)) then
        fid = fin
    else
        fid = 6     ! use stdout if not specified in input
    endif
    if (present(bin)) b = bin
    
    !
    WRITE(fid,*) "***********************************************************************"
    WRITE(fid,*) "*  Build information for "//FILE_NAME
    WRITE(fid,*) "*  "//FILE_DESCRIPTION
    
#ifdef __GFORTRAN__
    write (fid,*) "*  GFORTRAN, version ", __VERSION__ 
#endif
#ifdef __INTEL_COMPILER 
    write (fid,*) "*  Intel, version ",__INTEL_COMPILER,', ',__INTEL_COMPILER_BUILD_DATE
    !DEC$ IF DEFINED(_WIN32)
        !DEC$ IF DEFINED(_M_X64)
        WRITE(fid,*) "*  WINDOWS 64-bit" 
        !DEC$ ELSE
        WRITE(fid,*) "*  WINDOWS 32-bit" 
        !DEC$ ENDIF
    !DEC$ END IF
    !DEC$ IF DEFINED(__linux__)
        WRITE(fid,*) "*  Linux"
    !DEC$ ENDIF
    !DEC$ IF DEFINED(_DEBUG)
    WRITE(fid,*) "*  DEBUG version"
    !DEC$ ENDIF
    !DEC$ IF DEFINED(CLUSTER)
    WRITE(fid,*) "*  CLUSTER version"
    !DEC$ ENDIF
#endif
    WRITE(fid,*) "***********************************************************************"
    WRITE(fid,*) "*  GIT-TAG        = ", trim(b%git_tag)
    WRITE(fid,*) "*  GIT-BRANCH     = ", trim(b%git_branch)
    WRITE(fid,*) "*  BUILD_TYPE     = ", trim(b%build_type)
    WRITE(fid,*) "*  BUILDER        = ", trim(b%builder)
    WRITE(fid,*) "*  COMPUTER_NAME  = ", trim(b%computer_name)
    !WRITE(fid,*) "*  BUILD_TIME     = ", trim(b%build_time)
    WRITE(fid,*) "*  BUILD_DATE     = ", trim(b%build_date)
    WRITE(fid,*) "***********************************************************************"
    !
END SUBROUTINE
    
! Old stdcall version routine    
! subroutine version(s)
    ! implicit none
    ! !DEC$ ATTRIBUTES DLLEXPORT, ALIAS:'version' :: version
    ! !DEC$ ATTRIBUTES STDCALL :: version
    ! !DEC$ ATTRIBUTES REFERENCE:: s
    ! character*255      :: s
    ! type (tbuildinfo)  :: binfo
    ! call buildInfo_initialise(binfo)
    ! s = binfo%git_tag
! end subroutine

    
subroutine get_version(s) BIND(C, NAME='get_version')
    use iso_c_binding
    implicit none
#ifdef _WIN32 
    !!Windows (x64 and x86)
    !DEC$ ATTRIBUTES DLLEXPORT :: get_version
    !GCC$ ATTRIBUTES DLLEXPORT :: get_version
#endif
    character(kind=c_char, len=1), intent(inout)      :: s(255)
    integer :: i
    do i=1,len(GIT_ID)
        s(i) = GIT_ID(i:i)
    enddo
end subroutine


    
subroutine echo_version() BIND(C, NAME='echo_version')
    use iso_c_binding
    implicit none
#ifdef _WIN32 
    !!Windows (x64 and x86)
    !DEC$ ATTRIBUTES DLLEXPORT :: echo_version
    !GCC$ ATTRIBUTES DLLEXPORT :: echo_version
#endif
    type (tbuildinfo)   :: b
    call BuildInfo_echo(b)
end subroutine

subroutine startup_exe()
    CHARACTER(len=32) :: arg
    CALL get_command_argument(1, arg)
    IF (TRIM(arg) == "--version") then
        call echo_version()
        stop 0
    end if
end subroutine

END MODULE

