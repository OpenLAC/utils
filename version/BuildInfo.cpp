#include "version.h"
#include <string.h>
#include "export.h"
#include "file_info.h"
#include <iostream>
using namespace std;

extern "C" void EXPORT get_version(char* s){
	strncpy(s,  GIT_ID, 255);
}


extern "C" void EXPORT echo_version() {
    cout << "***********************************************************************\n";
    cout << "*  Build information for " << FILE_NAME << "\n";
    cout << "*  " << FILE_DESCRIPTION << "\n";

#ifdef __GNUC__
    cout << "*  GFORTRAN, version " << __VERSION__ << "\n";
#endif
#ifdef _MSC_VER 
    cout << "*  Microsoft C/C++, version " << _MSC_VER << "\n";
#endif
#ifdef __INTEL_COMPILER 
    //    write (fid,*) "*  Intel, version ",__INTEL_COMPILER,', ',__INTEL_COMPILER_BUILD_DATE
    //    !DEC$ IF DEFINED(_WIN32)
    //        !DEC$ IF DEFINED(_M_X64)
    //        WRITE(fid,*) "*  WINDOWS 64-bit" 
    //        !DEC$ ELSE
    //        WRITE(fid,*) "*  WINDOWS 32-bit" 
    //        !DEC$ ENDIF
    //    !DEC$ END IF
    //    !DEC$ IF DEFINED(__linux__)
    //        WRITE(fid,*) "*  Linux"
    //    !DEC$ ENDIF
    //    !DEC$ IF DEFINED(_DEBUG)
    //    WRITE(fid,*) "*  DEBUG version"
    //    !DEC$ ENDIF
    //    !DEC$ IF DEFINED(CLUSTER)
    //    WRITE(fid,*) "*  CLUSTER version"
    //    !DEC$ ENDIF
#endif
#ifdef _WIN32
    #ifdef _M_X64
        cout << "*  WINDOWS 64-bit\n";
    #else 
       cout << "*  WINDOWS 32-bit\n";
    #endif
#endif
#ifdef __linux__
        cout << "*  Linux\n";
#endif
#ifdef _DEBUG
        cout << "*  DEBUG version\n";
#endif

        cout << "***********************************************************************\n";
        cout << "*  GIT-TAG        = "<< GIT_ID<< "\n";
        cout << "*  BUILDER        = "<< USERNAME << "\n";
        cout << "*  COMPUTER_NAME  = "<<COMPUTERNAME << "\n"; 
        cout << "*  BUILD_DATE     = "<<BUILD_DATE << "\n"; 
        cout << "***********************************************************************" << "\n";
    

}

