
module StringUtils
    use ISO_C_BINDING, only: C_CHAR
   IMPLICIT NONE 
   CHARACTER( * ), PRIVATE, PARAMETER :: LOWER_CASE = 'abcdefghijklmnopqrstuvwxyz' 
   CHARACTER( * ), PRIVATE, PARAMETER :: UPPER_CASE = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' 
   public :: string2LowerCase, string2UpperCase
CONTAINS 
    subroutine cstring2fortran(s_c,s)
        !!! from https://gitlab.windenergy.dtu.dk/ebra/fortran-guidelines/blob/master/_tools/CStrings.f90         
        character(kind=C_CHAR,len=1),dimension(*),intent(in) :: s_c
        character(len=*),intent(inout):: s
        integer :: i
        loop_string: do i=1,len(s)
            if ( s_c(i) == CHAR(0) ) then
                exit loop_string
            else
                s(i:i) = s_c(i)
            end if
        end do loop_string

        if(i==1) then
            s=''
        else
            s = s(1:(i-1))
            s = trim(s)
        endif
    end subroutine
    
    subroutine fortranstring2c(s_f,s_c,n)
        !!! from https://gitlab.windenergy.dtu.dk/ebra/fortran-guidelines/blob/master/_tools/CStrings.f90   
        character(len=*),intent(in):: s_f
        character(kind=C_CHAR,len=1),dimension(*),intent(inout) :: s_c
        integer, intent(out), optional :: n  
        integer :: i
        loop_string: do i=1,len(s_f)
            if ( s_f(i:i) == CHAR(0) ) then
                exit loop_string
            else
                s_c(i) = s_f(i:i)
            end if
        end do loop_string
        if(present(n))then
            n=i-1
        endif
    end subroutine
    
    
 
   FUNCTION string2UpperCase ( Input_String ) RESULT ( Output_String ) 
     ! -- Argument and result 
     CHARACTER( * ), INTENT( IN )     :: Input_String 
     CHARACTER( LEN( Input_String ) ) :: Output_String 
     ! -- Local variables 
     INTEGER :: i, n 
     ! -- Copy input string 
     Output_String = Input_String 
     ! -- Loop over string elements 
     DO i = 1, LEN( Output_String ) 
       ! -- Find location of letter in lower case constant string 
       n = INDEX( LOWER_CASE, Output_String( i:i ) ) 
       ! -- If current substring is a lower case letter, make it upper case 
       IF ( n /= 0 ) Output_String( i:i ) = UPPER_CASE( n:n ) 
     END DO 
   END FUNCTION
   
   FUNCTION string2LowerCase ( Input_String ) RESULT ( Output_String ) 
     ! -- Argument and result 
     CHARACTER( * ), INTENT( IN )     :: Input_String 
     CHARACTER( LEN( Input_String ) ) :: Output_String 
     ! -- Local variables 
     INTEGER :: i, n 
     ! -- Copy input string 
     Output_String = Input_String 
     ! -- Loop over string elements 
     DO i = 1, LEN( Output_String ) 
       ! -- Find location of letter in upper case constant string 
       n = INDEX( UPPER_CASE, Output_String( i:i ) ) 
       ! -- If current substring is an upper case letter, make it lower case 
       IF ( n /= 0 ) Output_String( i:i ) = LOWER_CASE( n:n ) 
     END DO 
   END FUNCTION  
   
function replace(str,old_value,new_value)
    ! replace character
    character(len=*), intent(in) :: str
    character*1024 :: replace
    character*1 :: old_value, new_value
    integer :: i
    replace = ''
    do i=1, len_trim(str)
        if (str(i:i)==old_value) then
            replace(i:i) = new_value
        else
            replace(i:i) = str(i:i)
        endif
    enddo
end function

function count_char(str, char)
    character(len=*), intent(in) :: str
    character(len=1), intent(in) :: char
    integer :: count_char, i
    count_char = 0
    do i=1,len_trim(str)
        if (str(i:i)==char) count_char = count_char + 1
    end do
end function

function split(str, split_value, n)
    character(len=*), intent(in) :: str
    character(len=1), intent(in) :: split_value
    integer :: n, i, start_pos, end_pos 
    character(len=256) :: split(n+1)
    start_pos = 1
    do i=1,n
        split(i)=''
        end_pos = index(str(start_pos:), split_value) + start_pos -1
        split(i) = str(start_pos:end_pos-1)
        start_pos = end_pos + 1
    enddo
    split(n+1) = ''
    split(n+1) = trim(str(start_pos:))
end function
END MODULE
