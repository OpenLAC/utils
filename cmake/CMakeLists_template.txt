cmake_minimum_required (VERSION 3.0)
#Creating the project
project(<name> LANGUAGES Fortran)


# Adding all source files in ./source directory
file(GLOB_RECURSE SRC  *.f90)


set(BUILD_TYPE SHARED) # can be STATIC, EXE, SHARED
# set(LIB STATIC) # can be DLL or STATIC


include(${CMAKE_CURRENT_SOURCE_DIR}/utils/cmake/CMakeLists.txt)
