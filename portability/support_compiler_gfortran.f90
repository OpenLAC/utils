module SupportCompiler
    implicit none
    !
    integer, parameter :: RECORD_LENGTH=4  !< for direct access binaries
    !
    integer, parameter :: ISTR_LEN = 64 !< parameter for ease of comparison of parameter-strings
    character(len=ISTR_LEN), parameter :: FORTRAN_COMPILER='gfortran'

end module
